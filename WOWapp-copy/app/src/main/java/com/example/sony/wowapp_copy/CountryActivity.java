package com.example.sony.wowapp_copy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import POJO.Country;
import POJO.Models.CountryModel;
import Managers.AppManager;
import Managers.CountryManager;
import Views.CountryLayout;
import Views.NewCountryDialog;

/**
 * Created by sony on 09/07/2015.
 */
public class CountryActivity extends Activity {

    /**
     * The country model
     */
    private CountryModel countryModel;
    /**
     * The country layout
     */
    private CountryLayout countryLayout;

    CountryLayout.ViewListener viewListener = new CountryLayout.ViewListener() {

        /**
         * Modify the country model's list of countries
         * Replace the previous country model's list of countries with a new one that contains only
         * countries whose names start with the text written in the country name edit text
         */
        @Override
        public void onTextChanged() {
            String name = countryLayout.getCountryNameEditText();
            if (name != "") {
                ArrayList<Country> newList = new ArrayList<Country>();
                for (Country country : CountryManager.getInstance().getCountriesList())
                    if (country.getName().startsWith(name))
                        newList.add(country);
                countryModel.setCountryList(newList, true);
            }
        }


        @Override
        public void onAddNewCountryFloatingActionButtonClicked() {
            showNewCountryDialog();
        }

        /**
         * Method called when a country item in the list view is clicked
         * The current country activity finishes and values such as the
         * country's code, name and dial code are passed
         * to the previous activity that started the current one
         * @param position the position of the clicked item in the list
         */
        @Override
        public void onCountryItemClicked(int position) {
            Intent i = new Intent();
            i.putExtra("countryCode",
                    ((Country) countryLayout.getCountryAdapter().getItem(position)).getCode());
            i.putExtra("countryDialCode",
                    ((Country) countryLayout.getCountryAdapter().getItem(position)).getDial_code());
            i.putExtra("countryName",
                    ((Country) countryLayout.getCountryAdapter().getItem(position)).getName());
            setResult(RESULT_OK, i);
            finish();
        }
    };

    /**
     * Method called when the country activity is first created
     *
     * @param savedInstanceState the activity previously frozen state, if there was one
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initModel();
        initLayout();
        this.setContentView(this.countryLayout);
        countryLayout.setCountryModel(this.countryModel);
    }

    /**
     * Initialize the activity's model
     */
    private void initModel() {
        this.countryModel = new CountryModel();
        countryModel.setCountryList(AppManager.getInstance().getCountryManager().getCountriesList());
    }

    /**
     * Initialize the activity's layout
     */
    private void initLayout() {
        this.countryLayout = (CountryLayout) View.inflate(CountryActivity.this,
                R.layout.activity_country, null);
        countryLayout.setViewListener(viewListener);
    }

    /**
     * Initialize the contents of the Activity's standard options menu
     *
     * @param menu the options menu
     * @return true if the menu should be displayed and false otherwise
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Create and show a new country dialog
     */
    private void showNewCountryDialog() {
        final NewCountryDialog.Builder builder =
                new NewCountryDialog.Builder(CountryActivity.this);
        builder.setOnOkButtonClicked(new Runnable() {
            @Override
            public void run() {
                addNewCountry(builder.getCountryNameEditText(),
                        builder.getCountryCodeEditText(), builder.getDialCodeEditText());
                countryLayout.onChange();
            }
        });
        NewCountryDialog newCountryDialog = builder.build();
        newCountryDialog.show();
    }

    private void addNewCountry(String countryName, String countryCode, String dialCode) {
        AppManager.getInstance().getCountryManager()
                .getCountriesList().add(new Country(countryName, dialCode, countryCode));
    }
}
