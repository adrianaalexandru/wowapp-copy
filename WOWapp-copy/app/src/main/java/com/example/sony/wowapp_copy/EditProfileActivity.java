package com.example.sony.wowapp_copy;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import POJO.Country;
import POJO.Models.EditProfileModel;
import POJO.Models.ProfileModel;
import POJO.Phone;
import Utils.Constants;
import Views.EditProfileLayout;


/**
 * Created by sony on 12/07/2015.
 */
public class EditProfileActivity extends Activity {

    /**
     * GET_IMAGE_FROM_GALLERY code
     */
    public static final int GET_IMAGE_FROM_GALLERY = 1;
    /**
     * GET_COUNTRY code
     */
    public static final int GET_COUNTRY = 2;
    /**
     * The tag
     */
    public static final String TAG = "EDIT PROFILE ACTIVITY";

    /**
     * The profile layout
     */
    private EditProfileLayout editProfileLayout;
    /**
     * The profile model
     */
    private EditProfileModel editProfileModel;
    /**
     * The view listener
     */
    private EditProfileLayout.ViewListener v = new EditProfileLayout.ViewListener() {
        /**
         * Show a date picker dialog in order to select a new birthday
         */
        @Override
        public void onBirthdayTextViewClicked() {
            saveData();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(editProfileModel.getBirthday());
            DatePickerDialog datePickerDialog =
                    new DatePickerDialog(EditProfileActivity.this, dateSetListener,
                            calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH), calendar.get(calendar.YEAR));
            datePickerDialog.show();
        }

        /**
         * Show an alert dialog with a list of genders to select from
         */
        @Override
        public void onGenderTextViewClicked() {

            saveData();
            AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
            builder.setItems((String[]) Constants.genders.toArray(),
                    new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    editProfileModel.setGender(Constants.genders.get(item), true);
                }
            });
            AlertDialog alert = builder.create();

            alert.show();
        }

        /**
         * Start a new country activity
         */
        @Override
        public void onCountryTextViewClicked() {
            saveData();
            Intent i = new Intent(EditProfileActivity.this, CountryActivity.class);
            startActivityForResult(i, GET_COUNTRY);
        }

        /**
         * Show an alert dialog with a list of phone types to select from
         */
        @Override
        public void onPhoneTypeTextViewClicked() {
            saveData();
            AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
            builder.setItems((String[]) Constants.phoneTypes.toArray(),
                    new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    editProfileModel.setPhone(new Phone(editProfileModel.getPhone()
                            .getPhoneNumber(), Constants.phoneTypes.get(item)), true);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        /**
         * Save the view's current content into the edit profile model's fields
         * Pass the edit profile model to the previous activity and finish the current activity
         */
        @Override
        public void onSaveButtonClicked() {
            saveData();
            Intent intent = new Intent();
            intent.putExtra("editprofile", editProfileModel);
            setResult(RESULT_OK, intent);
            finish();
        }

        /**
         * Save the views' current content into the edit profile model's fields
         * Select an image from the gallery
         */
        @Override
        public void onProfilePicturePickerClicked() {
            saveData();
            getImageFromGallery();
        }

        /**
         * Finish the current activity with the canceled result
         */
        @Override
        public void onBackButtonClicked() {
            Intent intent = new Intent();
            setResult(RESULT_CANCELED, intent);
            finish();
        }

        /**
         * Start a new country activity
         */
        @Override
        public void onCountryCodeTextViewClicked() {
            saveData();
            Intent i = new Intent(EditProfileActivity.this, CountryActivity.class);
            startActivityForResult(i, GET_COUNTRY);
        }
    };

    /**
     * The date set listener
     */
    private DatePickerDialog.OnDateSetListener dateSetListener =
            new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, monthOfYear, dayOfMonth);
            Date date = calendar.getTime();
            editProfileModel.setBirthday(date, true);
        }
    };

    /**
     * Method called when the edit profile activity is first created
     *
     * @param savedInstanceState the activity previously frozen state, if there was one
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        ProfileModel value = null;
        if (extras != null) {
            value = (ProfileModel) extras.getSerializable("profile");
        }
        initModel(value);
        initLayout();
        this.setContentView(this.editProfileLayout);
        this.editProfileLayout.setEditProfileModel(this.editProfileModel);


    }

    /**
     * Initialize the edit profile layout
     */
    private void initLayout() {
        this.editProfileLayout = (EditProfileLayout) View.inflate(EditProfileActivity.this, R.layout.activity_edit_profile, null);
        this.editProfileLayout.setViewListener(v);

    }

    /**
     * Initialize the edit profile model
     *
     * @param value the profile model used to initialize the edit profile model's fields
     */
    private void initModel(ProfileModel value) {
        this.editProfileModel = new EditProfileModel();
        this.editProfileModel.setBirthday(new Date());
        this.editProfileModel.setCountry(new Country());
        this.editProfileModel.setPhone(new Phone());
        if (value != null) {
            this.editProfileModel.setCity(value.getCity());
            this.editProfileModel.setFirstName(value.getFirstName());
            this.editProfileModel.setLastName(value.getLastName());
            this.editProfileModel.setUsername(value.getUsername());
            this.editProfileModel.setGender(value.getGender());
            this.editProfileModel.setCountry(value.getCountry());
            this.editProfileModel.setPhone(value.getPhone());
            this.editProfileModel.setBirthday(value.getBirthday());
            this.editProfileModel.setProfilePicture(value.getProfilePicture());
            this.editProfileModel.setWebsite(value.getWebsite());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Select an image from the gallery
     */
    private void getImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GET_IMAGE_FROM_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (requestCode) {

                case GET_IMAGE_FROM_GALLERY:
                    if (resultCode == Activity.RESULT_OK) {
                        Uri selectedImage = data.getData();
                        this.editProfileModel.setProfilePicture(selectedImage.toString(), true);
                        break;
                    } else if (resultCode == Activity.RESULT_CANCELED) {
                        Log.e(TAG, "Selecting picture cancelled");
                    }
                    break;
                case GET_COUNTRY:
                    if (resultCode == Activity.RESULT_OK) {
                        editProfileModel.setCountry(new Country(data.getExtras()
                                .getString("countryName"), data.getExtras()
                                .getString("countryDialCode"),
                                data.getExtras().getString("countryCode")), true);
                    }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception in onActivityResult : " + e.getMessage());
        }
    }

    /**
     * Save the views' current content to the edit profile model's fields
     */
    public void saveData() {

        editProfileModel.setFirstName(editProfileLayout.getFirstNameEditText());
        editProfileModel.setLastName(editProfileLayout.getLastNameEditText());
        editProfileModel.setGender(editProfileLayout.getGenderTextView());
        editProfileModel.setWebsite(editProfileLayout.getWebsiteEditText());
        editProfileModel.setCity(editProfileLayout.getCityEditText());
        editProfileModel.setPhone(new Phone(editProfileLayout.getPhoneNumberEditText(), editProfileLayout.getPhoneTypeTextView()));
        try {
            editProfileModel.setBirthday(Constants.simpleDateFormat_ddMMyyyy.parse(editProfileLayout.getBirthdayTextView()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}