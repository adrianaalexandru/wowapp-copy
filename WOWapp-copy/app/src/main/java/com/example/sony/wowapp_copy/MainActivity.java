package com.example.sony.wowapp_copy;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import static com.example.sony.wowapp_copy.R.id.activity_main_profile_fab;


public class MainActivity extends ActionBarActivity {

    public static String TAG = "MainActivity";

    /**
     * The information text view
     */
    private TextView informationTextView;
    /**
     * The callback manager
     */
    private CallbackManager callbackManager;
    /**
     * The facebook callback
     */
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(final LoginResult loginResult) {
            Log.i(TAG, "Access token1: " + loginResult.getAccessToken().getToken());
            AccessToken accessToken = AccessToken.getCurrentAccessToken();
            Log.i(TAG, "Access token2: " + accessToken.getToken());

            final GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject me, GraphResponse response) {
                            if (response.getError() != null) {
                                Log.i(TAG, "error ");
                            } else {
                                String jsonresult = String.valueOf(me);
                                Log.i(TAG, "JSON Result" + jsonresult);
                                if (loginResult.getRecentlyGrantedPermissions().contains("email") == true) {
                                    String email = me.optString("email");
                                    Log.i(TAG, "email: " + email);
                                    informationTextView.append("email: " + email);
                                }
                                String gender = me.optString("gender");
                                Log.i(TAG, "gender: " + gender);
                                informationTextView.append("gender: " + gender);

                                String age_range = me.optString("age_range");
                                Log.i(TAG, "age_range: " + age_range);
                                informationTextView.append("age_range: " + age_range);

                                final String id = me.optString("id");
                                Log.i(TAG, "id: " + id);
                                informationTextView.append("id: " + id);

                                String locale = me.optString("locale");
                                Log.i(TAG, "locale: " + locale);
                                informationTextView.append("locale: " + locale);


                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,locale");
            request.setParameters(parameters);
            request.executeAsync();


            Profile profile = Profile.getCurrentProfile();
            if (profile != null)
                informationTextView.setText(profile.getFirstName() +
                        " " + profile.getLastName() + " " + profile.getName());


        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };


    /**
     * Method called when the main activity is first created
     *
     * @param savedInstanceState the activity previously frozen state, if there was one
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_main);
        initLayout();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Initialize the layout
     */
    private void initLayout() {

        informationTextView =
                (TextView) findViewById(R.id.activity_main_information_textview);
        Button profileButton =
                (Button) findViewById(R.id.activity_main_profile_button);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, FacebookActivity.class);
                startActivity(intent);
            }
        });
        Button notificationsButton =
                (Button) findViewById(R.id.activity_main_notifications_button);
        notificationsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(intent);
            }
        });
        FloatingActionButton profileFloatingActionButton =
                (FloatingActionButton) findViewById(activity_main_profile_fab);
        profileFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        LoginButton loginButton =
                (LoginButton) findViewById(R.id.activity_main_login_button);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile", "user_friends"));

        loginButton.registerCallback(callbackManager, callback);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Get the current profile picture of the user with the id userID
     *
     * @param userID the user id
     * @return the user's profile picture as a Bitmap
     */
    public static Bitmap getFacebookProfilePicture(String userID) {
        try {
            URL imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=large");
            Bitmap bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
            return bitmap;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
