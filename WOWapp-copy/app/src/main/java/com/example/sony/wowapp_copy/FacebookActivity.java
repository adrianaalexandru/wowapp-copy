package com.example.sony.wowapp_copy;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.ProfilePictureView;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by sony on 29/07/2015.
 */
public class FacebookActivity extends Activity {
    private enum PendingAction {
        NONE,
        POST_PHOTO,
        POST_STATUS_UPDATE
    }

    /**
     * The permissions for facebook
     */
    private static final String PERMISSION = "publish_actions";
    /**
     * The current pending action
     */
    private PendingAction pendingAction;
    /**
     * The post photo button
     */
    private Button postPhotoButton;
    /**
     * The post status update button
     */
    private Button postStatusUpdateButton;
    /**
     * The name text view
     */
    private TextView nameTextView;
    /**
     * The profile picture view
     */
    private ProfilePictureView profilePictureView;
    /**
     * The callback manager
     */
    private CallbackManager callbackManager;
    /**
     * The share dialog
     */
    private ShareDialog shareDialog;
    /**
     * The share facebook callback
     */
    private FacebookCallback<Sharer.Result> shareCallback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onCancel() {
            Log.d("HelloFacebook", "Canceled");
        }

        @Override
        public void onError(FacebookException error) {
            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
            String title = getString(R.string.error);
            String alertMessage = error.getMessage();
            showResult(title, alertMessage);
        }

        @Override
        public void onSuccess(Sharer.Result result) {
            Log.d("HelloFacebook", "Success!");
            if (result.getPostId() != null) {
                String title = getString(R.string.success);
                String id = result.getPostId();
                String alertMessage = getString(R.string.successfully_posted_post, id);
                showResult(title, alertMessage);
            }
        }

        private void showResult(String title, String alertMessage) {
            new AlertDialog.Builder(FacebookActivity.this)
                    .setTitle(title)
                    .setMessage(alertMessage)
                    .setPositiveButton(R.string.ok, null)
                    .show();
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handlePendingAction();
                        updateUI();
                    }

                    @Override
                    public void onCancel() {
                        if (pendingAction != PendingAction.NONE) {
                            showAlert();
                            pendingAction = PendingAction.NONE;
                        }
                        updateUI();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if (pendingAction != PendingAction.NONE
                                && exception instanceof FacebookAuthorizationException) {
                            showAlert();
                            pendingAction = PendingAction.NONE;
                        }
                        updateUI();
                    }

                    private void showAlert() {
                        new AlertDialog.Builder(FacebookActivity.this)
                                .setTitle("Cancelled")
                                .setMessage("Permission not granted")
                                .setPositiveButton("OK", null)
                                .show();
                    }
                });
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(
                callbackManager,
                shareCallback);


        setContentView(R.layout.facebookshare_activity);
        initLayout();

    }

    /**
     * Initialize the layout
     */
    private void initLayout() {
        profilePictureView = (ProfilePictureView) findViewById(R.id.facebookshare_activity_profilepicture);
        nameTextView = (TextView) findViewById(R.id.facebookshare_activity_name_textview);
        postPhotoButton = (Button) findViewById(R.id.facebookshare_activity_post_photo_button);
        postStatusUpdateButton = (Button) findViewById(R.id.facebookshare_activity_post_status_button);

        postStatusUpdateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                pendingAction = PendingAction.POST_STATUS_UPDATE;
                postStatusUpdate();
            }
        });

        postPhotoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                pendingAction = PendingAction.POST_PHOTO;
                postPhoto();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Post a photo on Facebook
     * The user must be logged in, in order for the content to be shared
     * If the user is not logged in or he didn't grant publish permissions,
     * he/she must do that before the action of posting the photo is completed
     */
    private void postPhoto() {
        Bitmap image = BitmapFactory.decodeResource(this.getResources(), R.drawable.pink_earth);
        SharePhoto sharePhoto = new SharePhoto.Builder().setBitmap(image).build();
        ArrayList<SharePhoto> photos = new ArrayList<>();
        photos.add(sharePhoto);

        SharePhotoContent sharePhotoContent =
                new SharePhotoContent.Builder().setPhotos(photos).build();
        if (shareDialog.canShow(SharePhotoContent.class)) {
            if (hasPublishPermission() && Profile.getCurrentProfile() != null) {
                shareDialog.show(sharePhotoContent);
                pendingAction = PendingAction.NONE;
            } else {
                LoginManager.getInstance().logInWithPublishPermissions(
                        this,
                        Arrays.asList(PERMISSION));
            }
        }
    }

    /**
     * Post a status update on Facebook
     * The user must be logged in, in order for the content to be shared
     * If the user is not logged in or he didn't grant publish permissions,
     * he/she must do that before the action of posting the status update is completed
     */
    private void postStatusUpdate() {

        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentTitle("Adriana")
                .setContentDescription(
                        "The 'Hello Facebook' sample  showcases simple Facebook integration")
                .setContentUrl(Uri.parse("http://developers.facebook.com/docs/android"))
                .build();
        if (shareDialog.canShow(ShareLinkContent.class)) {
            if (hasPublishPermission() && Profile.getCurrentProfile() != null) {
                shareDialog.show(linkContent);
                pendingAction = PendingAction.NONE;
            } else {
                LoginManager.getInstance().logInWithPublishPermissions(
                        this,
                        Arrays.asList(PERMISSION));
            }
        }
    }

    /**
     * Handle the pending action
     */
    private void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        pendingAction = PendingAction.NONE;

        switch (previouslyPendingAction) {
            case NONE:
                break;
            case POST_PHOTO:
                postPhoto();
                break;
            case POST_STATUS_UPDATE:
                postStatusUpdate();
                break;
        }
    }

    /**
     * Check if the the current user logged in granted publish permission
     *
     * @return true if there is a current user logged in and he/she has allowed publish permission,
     * false otherwise
     */
    private boolean hasPublishPermission() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null && accessToken.getPermissions().contains("publish_actions");
    }

    /**
     * Update the layout's views
     */
    private void updateUI() {
        Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            profilePictureView.setProfileId(profile.getId());
            nameTextView.setText(profile.getFirstName());
        } else {
            profilePictureView.setProfileId(null);
            nameTextView.setText(null);
        }
    }

}

