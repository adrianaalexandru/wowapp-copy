package com.example.sony.wowapp_copy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import POJO.Models.StatusModel;
import Views.StatusLayout;

/**
 * Created by sony on 09/07/2015.
 */
public class StatusActivity extends Activity {
    /**
     * The status layout
     */
    private StatusLayout statusLayout;
    /**
     * The status model
     */
    private StatusModel statusModel;
    /**
     * The status view listener
     */
    private StatusLayout.StatusViewListener statusViewListener=new StatusLayout.StatusViewListener() {
        /**
         * Finish the current status activity and pass the model's status to the previous activity
         */
        @Override
        public void onSaveStatusButtonClick() {
            Intent resultIntent = new Intent();
            resultIntent.putExtra("statusString",getStatusModel().getStatus());
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }
    };


    /**
     * Method called when the status activity is first created
     * contains code to initialize the activity
     * @param savedInstanceState the activity previously frozen state, if there was one
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initModel();
        initLayout();
        this.setContentView(statusLayout);
        statusLayout.setStatusModel(statusModel);
    }
    /**
     * Initialize the layout
     */
    private void initLayout(){

        statusLayout=
                (StatusLayout) View.inflate(StatusActivity.this,R.layout.activity_status, null);
        statusLayout.setStatusViewListener(statusViewListener);
    }

    /**
     *Initialize the model
     */
    private void initModel(){
        statusModel=new StatusModel();
    }
    /**
     * Initialize the contents of the Activity's standard options menu
     * @param menu the options menu
     * @return true if the menu should be displayed and false otherwise
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Get the status model
     * @return the status model
     */
    public StatusModel getStatusModel() {
        return statusModel;
    }

    /**
     * Set a new status model for the activity
     * @param statusModel the new status model
     */
    public void setStatusModel(StatusModel statusModel) {
        this.statusModel = statusModel;
    }
}
