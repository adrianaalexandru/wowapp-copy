package com.example.sony.wowapp_copy;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import POJO.Models.NotificationModel;
import Views.NotificationDialog;
import Views.NotificationLayout;

/**
 * Created by sony on 19/07/2015.
 */
public class NotificationActivity extends Activity {

    /**
     * The model
     */
    private NotificationModel notificationModel;
    /**
     * The layout
     */
    private NotificationLayout notificationLayout;

    /**
     * The view listener
     */
    NotificationLayout.ViewListener viewListener=new NotificationLayout.ViewListener() {
        @Override
        public void onMessagesLinearLayoutClicked() {
            showNotificationDialog(getResources().getString(R.string.Messages),
                    getNotificationModel().getMessagesNotifications());
        }

        @Override
        public void onCallsLinearLayoutClicked() {
            showNotificationDialog(getResources().getString(R.string.Calls),
                    getNotificationModel().getCallsNotifications());
        }

        @Override
        public void onEarningsLinearLayoutClicked() {
            showNotificationDialog(getResources().getString(R.string.Earnings),
                    getNotificationModel().getEarningsNotifications());
        }

        @Override
        public void onNetworkLinearLayoutClicked() {
            showNotificationDialog(getResources().getString(R.string.Network),
                    getNotificationModel().getNetworkNotifications());
        }
    };

    /**
     * Method called when the notifications activity is first created
     * @param savedInstanceState the activity previously frozen state, if there was one
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initModel();
        initLayout();
        this.setContentView(this.notificationLayout);
        notificationLayout.setNotificationModel(this.notificationModel);

    }

    /**
     * Create and show a new notification dialog with the title and notifications' list given
     * @param title The title for the dialog
     * @param notificationsList The notifications' list for the dialog
     */
    private void showNotificationDialog(final String title, ArrayList<String> notificationsList)
    {
        final NotificationDialog.Builder builder = new NotificationDialog.Builder(NotificationActivity.this);
        builder.setTitle(title);
        builder.setNotificationsList(notificationsList);
        builder.setOnOkButtonClicked(new Runnable() {
            @Override
            public void run() {
                changeNotificationsList(title, builder.getNotificationsList());
            }
        });
        NotificationDialog notificationDialog=builder.build();
        notificationDialog.show();
    }
    /**
     * Initialize the activity's model
     */
    private void initModel(){
        this.notificationModel=new NotificationModel();
    }

    /**
     * Initialize the activity's layout
     */
    private void initLayout(){
        this.notificationLayout = (NotificationLayout) View.inflate(NotificationActivity.this,
                R.layout.activity_notifications, null);
        notificationLayout.setViewListener(viewListener);
    }
    /**
     * Initialize the contents of the Activity's standard options menu
     * @param menu the options menu
     * @return true if the menu should be displayed and false otherwise
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Get the notification model
     * @return The notification model
     */
    public NotificationModel getNotificationModel(){
        return this.notificationModel;
    }

    /**
     * Change the model notifications' list according to the title given
     *
     * @param title The title
     * @param newNotificationsList The new notifications' list
     */
    public void changeNotificationsList(String title,ArrayList<String> newNotificationsList) {
       if (title==getResources().getString(R.string.Network))
           this.getNotificationModel().setNetworkNotifications(newNotificationsList, true);
        else if (title==getResources().getString(R.string.Messages))
           this.getNotificationModel().setMessagesNotifications(newNotificationsList, true);
        else if(title==getResources().getString(R.string.Earnings))
           this.getNotificationModel().setEarningsNotifications(newNotificationsList, true);
        else if (title==getResources().getString(R.string.Calls))
           this.getNotificationModel().setCallsNotifications(newNotificationsList, true);

    }
}
