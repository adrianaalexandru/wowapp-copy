package com.example.sony.wowapp_copy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import POJO.Models.EditProfileModel;
import POJO.Models.ProfileModel;
import Views.ProfileLayout;

/**
 * Created by sony on 09/07/2015.
 */
public class ProfileActivity extends Activity {

    /**
     * GET_STATUS code
     */
    public static final int GET_STATUS = 1;
    /**
     * EDIT_PROFILE code
     */
    public static final int EDIT_PROFILE = 2;
    /**
     * The profile layout
     */
    private ProfileLayout profileLayout;
    /**
     * The profile model
     */
    private ProfileModel profileModel;
    /**
     * The view listener
     */
    private ProfileLayout.ViewListener v = new ProfileLayout.ViewListener() {
        /**
         * Start for result a new edit profile activity and pass the profile model to the second activity
         */
        @Override
        public void OnEditButtonClicked() {

            Intent i = new Intent(ProfileActivity.this, EditProfileActivity.class);
            i.putExtra("profile", profileModel);
            startActivityForResult(i, 2);
        }

        /**
         * Start for result a new status activity
         */
        @Override
        public void OnStatusTextViewClicked() {
            Intent i = new Intent(ProfileActivity.this, StatusActivity.class);
            startActivityForResult(i, 1);
        }
    };

    /**
     * Method called when the profile activity is first created
     *
     * @param savedInstanceState the activity previously frozen state, if there was one
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initModel();
        initLayout();
        this.setContentView(this.profileLayout);
        this.profileLayout.setProfileModel(this.profileModel);
    }

    /**
     * Initialize the layout
     */
    private void initLayout() {
        this.profileLayout =
                (ProfileLayout) View.inflate(ProfileActivity.this, R.layout.activity_profile, null);
        this.profileLayout.setViewListener(v);
    }

    /**
     * Initialize the model
     */
    private void initModel() {
        this.profileModel = new ProfileModel();
        this.profileModel.setUsername("username1");
        this.profileModel.setEmail("adresa_email@yahoo.com");
    }

    /**
     * Initialize the contents of the Activity's standard options menu
     *
     * @param menu the options menu
     * @return true if the menu should be displayed and false otherwise
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Change the values of the profile model's fields with the values from the edit profile model fields
     *
     * @param editProfileModel the edit profile model
     * @param profileModel     the profile model
     */
    private void editProfileToProfile(EditProfileModel editProfileModel, ProfileModel profileModel) {
        profileModel.setUsername(editProfileModel.getUsername(), true);
        profileModel.setFirstName(editProfileModel.getFirstName(), true);
        profileModel.setGender(editProfileModel.getGender(), true);
        profileModel.setLastName(editProfileModel.getLastName(), true);
        profileModel.setProfilePicture(editProfileModel.getProfilePicture(), true);
        profileModel.setCity(editProfileModel.getCity(), true);
        profileModel.setCountry(editProfileModel.getCountry(), true);
        profileModel.setBirthday(editProfileModel.getBirthday(), true);
        profileModel.setPhone(editProfileModel.getPhone(), true);
        profileModel.setWebsite(editProfileModel.getWebsite(), true);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GET_STATUS)
            if (resultCode == RESULT_OK) {
                String statusString = data.getStringExtra("statusString");
                this.profileModel.setStatus(statusString, true);
            }
        if (requestCode == EDIT_PROFILE) {
            if (resultCode == RESULT_OK) {
                EditProfileModel editProfileModel =
                        (EditProfileModel) data.getSerializableExtra("editprofile");
                editProfileToProfile(editProfileModel, this.profileModel);

            }
        }


    }
}
