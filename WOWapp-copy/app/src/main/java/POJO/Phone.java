package POJO;

import java.io.Serializable;

import Utils.Constants;

/**
 * Created by sony on 09/07/2015.
 */
public class Phone implements Serializable {

    /**
     * The telephone number
     */
    private String phoneNumber;
    /**
     * The telephone type : Home, Mobile, Office, Virtual
     */
    private String phoneType;

    /**
     * The phone's constructor without parameters
     */
    public Phone(){
        this.phoneNumber="";
        this.phoneType="Mobile";
    }
    /**
     * The phone's constructor with parameters
     */
    public Phone(String phoneNumber,String phoneType)
    {
        this.phoneNumber=phoneNumber;
        this.phoneType=phoneType;
    }
    /**
     * Get the telephone number
     * @return the telephone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Set a new value for the telephone number
     * @param phoneNumber the new value for the field phone number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Get the phone number type
     * @return the phone number type
     */
    public String getPhoneType() {
        return phoneType;
    }


    /**
     * Set a new value for the phone type.The new value has to be Mobile, Home, Office or Virtual in order for the change to be made
     * @param phoneType the new value for the phone type
     * @return true if the value is successfully set and false otherwise
     */
    public boolean setPhoneType(String phoneType) {
        if(Constants.phoneTypes.contains(phoneType)==true) {
            this.phoneType = phoneType;
            return true;
        }
        return false;
    }
}
