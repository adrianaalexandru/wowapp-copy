package POJO.Models;

import POJO.Interfaces.SimpleObservable;

/**
 * Created by sony on 09/07/2015.
 */
public class StatusModel extends SimpleObservable<StatusModel> {

    /**
     * The status
     */
    private String status;


    /**
     * Get the status
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set a new status
     * @param status the new status
     * @param isNotificationRequired
     */
    public void setStatus(String status,boolean ... isNotificationRequired) {
        this.status = status;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }
}
