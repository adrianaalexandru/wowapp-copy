package POJO.Models;

import java.io.Serializable;
import java.util.Date;

import POJO.Country;
import POJO.Interfaces.SimpleObservable;
import POJO.Phone;

/**
 * Created by sony on 12/07/2015.
 */
public class EditProfileModel extends SimpleObservable<EditProfileModel> implements Serializable {


    /**
     * The profile picture's Uri as a String
     */
    private String profilePicture;
    /**
     * The first name
     */
    private String firstName;
    /**
     * The last name
     */
    private String lastName;
    /**
     * The username
     */
    private String username;
    /**
     * The birthday
     */
    private Date birthday;
    /**
     * The gender: Female or Male
     */
    private String gender;
    /**
     * The country
     */
    private Country country;
    /**
     * The city
     */
    private String city;
    /**
     * The phone
     */
    private Phone phone;
    /**
     * The website
     */
    private String website;



    /**
     * Get the website
     * @return the website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * Set a new website
     * @param website the new website
     * @param isNotificationRequired
     */
    public void setWebsite(String website,boolean ... isNotificationRequired) {
        this.website = website;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the phone
     * @return the phone
     */
    public Phone getPhone() {
        return phone;
    }

    /**
     * Set a new value for the phone
     * @param phone the new phone
     * @param isNotificationRequired
     */
    public void setPhone(Phone phone,boolean ... isNotificationRequired) {
        this.phone = phone;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();

    }
    /**
     * Get the city
     * @return the city
     */
    public String getCity() {
        return city;
    }
    /**
     * Set a new city
     * @param city the new value to be set for the city
     * @param isNotificationRequired
     */
    public void setCity(String city,boolean ... isNotificationRequired) {
        this.city = city;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }
    /**
     * Get the country
     * @return
     */
    public Country getCountry() {
        return country;
    }
    /**
     * Set the country
     * @param country the new country
     * @param isNotificationRequired
     */
    public void setCountry(Country country,boolean ... isNotificationRequired) {

            this.country = country;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }
    /**
     * Get the gender
     * @return the gender
     */
    public String getGender() {
        return gender;
    }
    /**
     * Set a new gender
     * @param gender the new gender
     * @param isNotificationRequired
     */
    public void setGender(String gender,boolean ... isNotificationRequired) {

        this.gender = gender;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the username
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set a new username
     * @param username the new username
     * @param isNotificationRequired
     */
    public void setUsername(String username,boolean ... isNotificationRequired) {
        this.username = username;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the birthday
     * @return the birthday
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * Set a new birthday
     * @param birthday the new birthday
     * @param isNotificationRequired
     */
    public void setBirthday(Date birthday,boolean ... isNotificationRequired) {
        this.birthday = birthday;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the last name
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set a new last name
     * @param lastName the new last name
     * @param isNotificationRequired
     */
    public void setLastName(String lastName,boolean ... isNotificationRequired) {
        this.lastName = lastName;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the first name
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set a new first name
     * @param firstName the new first name
     * @param isNotificationRequired
     */
    public void setFirstName(String firstName,boolean ... isNotificationRequired) {
        this.firstName = firstName;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     *Get the profile picture's Uri as a String
     * @return the profile picture's Uri as a String
     */
    public String getProfilePicture() {
        return profilePicture;
    }

    /**
     * Set a new profile picture Uri
     * @param profilePicture the new profile picture Uri
     * @param isNotificationRequired
     */
    public void setProfilePicture(String profilePicture,boolean ... isNotificationRequired)  {
        this.profilePicture = profilePicture;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }
}
