package POJO.Models;

import java.io.Serializable;
import java.util.Date;

import POJO.Country;
import POJO.Interfaces.SimpleObservable;
import POJO.Phone;
import Utils.Constants;

/**
 * Created by sony on 09/07/2015.
 */
public class ProfileModel extends SimpleObservable<ProfileModel> implements Serializable {

    /**
     * The profile picture's Uri as a String
     */
    private String profilePicture;
    /**
     * The first name
     */
    private String firstName;
    /**
     * The last name
     */
    private String lastName;
    /**
     * The username
     */
    private String username;
    /**
     * The birthday
     */
    private Date birthday;
    /**
     * The gender: Female or Male
     */
    private String gender;
    /**
     * The country
     */
    private Country country;
    /**
     * The city
     */
    private String city;
    /**
     * The email
     */
    private String email;
    /**
     * The phone
     */
    private Phone phone;
    /**
     * The status
     */
    private String status;
    /**
     * The website
     */
    private String website;

    /**
     * The profile model constructor
     */
    public ProfileModel() {
        this.birthday=new Date();
        this.city="";
        this.firstName="";
        this.lastName="";
        this.email="";
        this.username="";
        this.phone=new Phone();
        this.country=new Country();
        this.gender="";
        this.status="";
        this.profilePicture=null;

    }

    /**
     * Get the first name
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set a new first name
     * @param firstName the new fist name
     * @param isNotificationRequired
     */
    public void setFirstName(String firstName,boolean ... isNotificationRequired) {
        this.firstName = firstName;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the last name
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set a new last name
     * @param lastName the new last name
     * @param isNotificationRequired
     */
    public void setLastName(String lastName,boolean ... isNotificationRequired) {
        this.lastName = lastName;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the username
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set a new username
     * @param username the new username
     * @param isNotificationRequired
     */
    public void setUsername(String username,boolean ... isNotificationRequired) {
        this.username = username;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the birthday
     * @return the birthday
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * Set a new birthday
     * @param birthday the new birthday
     * @param isNotificationRequired
     */
    public void setBirthday(Date birthday,boolean ... isNotificationRequired) {
        this.birthday = birthday;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     *Get the gender
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * Set a new value for the gender field. New new value has to be Female or Male in order for the change to be made successfully
     * @param gender the new gender
     * @return true if the new value has been successfully set and false otherwise
     */
    public boolean setGender(String gender,boolean ... isNotificationRequired) {
        if (Constants.genders.contains(gender)==true) {
            this.gender = gender;
            if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
                notifyObservers();
            return true;
        }
        return false;
    }

    /**
     * Get the country
     * @return the country
     */
    public Country getCountry() {
        return country;
    }

    /**
     * Set a new country
     * @param country the new country
     * @param isNotificationRequired
     */
    public void setCountry(Country country,boolean ... isNotificationRequired) {
        this.country = country;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the city
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Set a new city
     * @param city the new city
     * @param isNotificationRequired
     */
    public void setCity(String city,boolean ... isNotificationRequired) {
        this.city = city;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the email
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set a new email
     * @param email the new email
     * @param isNotificationRequired
     */
    public void setEmail(String email,boolean ... isNotificationRequired) {
        this.email = email;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the phone
     * @return the phone
     */
    public Phone getPhone() {
        return phone;
    }

    /**
     * Set a new phone
     * @param phone a new phone
     * @param isNotificationRequired
     */
    public void setPhone(Phone phone,boolean ... isNotificationRequired) {
        this.phone = phone;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the status
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set a new status
     * @param status a new status
     * @param isNotificationRequired
     */
    public void setStatus(String status,boolean ... isNotificationRequired) {
        this.status = status;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the website
     * @return the website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * Set a new website
     * @param website the new website
     * @param isNotificationRequired
     */
    public void setWebsite(String website,boolean ... isNotificationRequired)  {
        this.website = website;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the profile picture's Uri as a String
     * @return the profile picture's Uri as a String
     */
    public String getProfilePicture() {
        return profilePicture;
    }

    /**
     * Set a new profile picture Uri
     * @param profilePicture
     * @param isNotificationRequired
     */
    public void setProfilePicture(String profilePicture,boolean ... isNotificationRequired)  {
        this.profilePicture = profilePicture;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }
}
