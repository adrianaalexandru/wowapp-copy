package POJO.Models;

import java.util.ArrayList;

import POJO.Interfaces.SimpleObservable;

/**
 * Created by sony on 16/07/2015.
 */
public class NotificationModel extends SimpleObservable<NotificationModel> {

    /**
     * The list of notifications for messages
     */
    private ArrayList<String> messagesNotifications=new ArrayList<String>();
    /**
     * The list of notifications for calls
     */
    private ArrayList<String> callsNotifications=new ArrayList<String>();
    /**
     * The list of notifications for earnings
     */
    private ArrayList<String> earningsNotifications=new ArrayList<String>();
    /**
     * The list of notifications for network
     */
    private ArrayList<String> networkNotifications=new ArrayList<String>();




    /**
     * Get the list of  notifications for messages
     * @return the list of  notifications for messages
     */
    public ArrayList<String> getMessagesNotifications() {
        return messagesNotifications;
    }

    /**
     * Set a new list of notifications for messages
     * @param messagesNotifications the new list of notifications for messages
     */
    public void setMessagesNotifications(ArrayList<String> messagesNotifications,boolean ... isNotificationRequired) {
        this.messagesNotifications = messagesNotifications;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the list of  notifications for calls
     * @return the list of  notifications for calls
     */
    public ArrayList<String> getCallsNotifications() {
        return callsNotifications;
    }

    /**
     * Set a new list of notifications for calls
     * @param callsNotifications the new list of notifications for calls
     */
    public void setCallsNotifications(ArrayList<String> callsNotifications,boolean ... isNotificationRequired) {
        this.callsNotifications = callsNotifications;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the list of  notifications for earnings
     * @return the list of  notifications for earnings
     */
    public ArrayList<String> getEarningsNotifications() {
        return earningsNotifications;
    }

    /**
     * Set a new list of notifications for earnings
     * @param earningsNotifications the new list of notifications for earnings
     */
    public void setEarningsNotifications(ArrayList<String> earningsNotifications,boolean ... isNotificationRequired) {
        this.earningsNotifications = earningsNotifications;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }

    /**
     * Get the list of  notifications for network
     * @return the list of  notifications for network
     */
    public ArrayList<String> getNetworkNotifications() {
        return networkNotifications;
    }

    /**
     * Set a new list of notifications for network
     * @param networkNotifications the new list of notifications for network
     */
    public void setNetworkNotifications(ArrayList<String> networkNotifications,boolean ... isNotificationRequired) {
        this.networkNotifications = networkNotifications;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }
}
