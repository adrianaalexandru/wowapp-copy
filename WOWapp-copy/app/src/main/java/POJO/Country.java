package POJO;


import java.io.Serializable;

/**
 * Created by sony on 09/07/2015.
 */
public class Country implements Serializable{

    /**
     * The country's name
     */
    private String name;
    /**
     * The country's prefix code/ calling code
     */
    private String dial_code;
    /**
     * The country's code
     */
    private String code;


    /**
     * The country's constructor without parameters
     */
    public Country (){
        this.name="";
        this.dial_code="";
        this.code="";
    }
    /**
     * The country's constructor with parameters
     */
    public Country(String name,String dial_code,String code){
        this.name=name;
        this.dial_code=dial_code;
        this.code=code;

    }

    /**
     * Get the country's name
     * @return the country's name
     */
    public String getName() {
        return name;
    }

    /**
     * Set a new value for the country's name
     * @param countryName the new value for the country's name
     */
    public void setName(String countryName) {
        this.name =name;
    }

    /**
     * Get the country's prefix code
     * @return the country's prefix code
     */
    public String getDial_code() {
        return dial_code;
    }

    /**
     * Set a new value for the country's prefix code
     * @param dial_code the new value
     */
    public void setDial_code(String dial_code) {
        this.dial_code = dial_code;
    }

    /**
     * Get the country's code
     * @return the country's code
     */
    public String getCode() {
        return code;
    }

    /**
     * Set a new value for the country's code
     * @param code the new value for the country's code
     */
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object object)
    {
        boolean same = false;

        if (object != null && object instanceof Country)
        {

            same = this.name.equals(((Country) object).name);
        }

        return same;
    }}