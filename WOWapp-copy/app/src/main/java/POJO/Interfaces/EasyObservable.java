package POJO.Interfaces;

/**
 * Created by sony on 09/07/2015.
 */

public interface EasyObservable <T> {

    /**
     * Adds the listener.
     *
     * @param listener the listener
     */
    void addListener(OnChangeListener<T> listener);

    /**
     * Removes the listener.
     *
     * @param listener the listener
     */
    void removeListener(OnChangeListener<T> listener);

}