package Managers;

/**
 * Created by sony on 15/07/2015.
 */
public class AppManager {
    private static volatile AppManager instance;
    private CountryManager countryManager;


    private AppManager(){

        countryManager=CountryManager.getInstance();
    }
    public static AppManager getInstance(){
        if(instance==null)
        {
            synchronized (CountryManager.class)
            {
                if(instance==null)
                    instance=new AppManager();

            }
        }
        return instance;
    }
    public CountryManager getCountryManager(){
        return countryManager;
    }
}
