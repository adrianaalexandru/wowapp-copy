package Managers;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.client.ClientProtocolException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import POJO.Country;

/**
 * Created by sony on 14/07/2015.
 */
public class CountryManager {
    private static volatile CountryManager instance;
    /**
     * the countries' list
     */
    private ArrayList<Country> countriesList=new ArrayList<Country>();


    private CountryManager()
    {
        sendGet();
    }
    public static CountryManager getInstance(){
         if(instance==null)
        {
            synchronized (CountryManager.class)
            {
                if(instance==null)
                 instance=new CountryManager();

            }
         }
        return instance;
    }


    public ArrayList<Country> getCountriesList() {

        return this.countriesList;
    }

    private void sendGet()
    {
        Log.i("CountryManager", "sending get");
        String URL = "https://gist.githubusercontent.com/Goles/3196253/raw/1c2b972438c88480b23bdb44c0469bc56010d470/CountryCodes.json";
        RequestTask requestTask = new RequestTask();
        requestTask.execute(URL);

        Log.i("CountryManager", "executed");
    }


    /**
     * the requesttask class
     * gets a request
     */
    class RequestTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... uri) {
            StringBuilder sb = new StringBuilder();
            try {

                URL url = new URL(uri[0]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                if(conn.getResponseCode() == HttpsURLConnection.HTTP_OK){
                    // Do normal input or output stream reading
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line+"\n");
                    }
                    br.close();

                }
                else {
                    Log.i("MainActivity", "failed");
                }
            } catch (ClientProtocolException e) {
                //TODO Handle problems..
            } catch (IOException e) {
                //TODO Handle problems..
            }
            return sb.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Gson gson=new Gson();
            try {
                Country [] countriesListFromJson = gson.fromJson(result, Country[].class);
                for(Country country :countriesListFromJson)
                    countriesList.add(country);

            }
            catch (Exception e) {
                Log.i("CountryManager","error");
            }


        }
    }


}
