package Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sony on 09/07/2015.
 */
public class Constants {


    public static List<String> phoneTypes= Arrays.asList("Home","Mobile","Office","Virtual");
    public static List<String> genders=Arrays.asList("Female","Male");
    public static SimpleDateFormat simpleDateFormat_MMMddyyyy=new SimpleDateFormat("MMM dd,yyyy");
    public static SimpleDateFormat simpleDateFormat_ddMMyyyy=new SimpleDateFormat("dd/MM/yyyy");

}
