package Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.sony.wowapp_copy.R;

import POJO.Interfaces.OnChangeListener;
import POJO.Models.StatusModel;

/**
 * Created by sony on 09/07/2015.
 */
public class StatusLayout extends RelativeLayout implements OnChangeListener<StatusModel> {

    /**
     * The status edit text
     */
   private EditText statusEditText;
    /**
     * The save status button
     */
   private Button saveStatusButton;
    /**
     *
     * The status model
     */
   private StatusModel statusModel;

    /**
     * The view listener
     */
   private StatusViewListener statusViewListener;

    /**
     * The status layout's constructor
     * @param context
     * @param attrs
     */
    public StatusLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initLayout();
    }

    /**
     * Initialize the layout
     */
    private void initLayout(){

        saveStatusButton= (Button) findViewById(R.id.activity_status_save_status_button);
        statusEditText= (EditText) findViewById(R.id.status_activity_status_edittext );

        saveStatusButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getStatusModel().setStatus(statusEditText.getText().toString());
                statusViewListener.onSaveStatusButtonClick();

            }
        });
    }
    @Override
    public void onChange() {
    updateLayout();
    }
    /**
     * Update the layout
     */
    private void updateLayout(){
        this.statusEditText.setText(this.getStatusModel().getStatus());
    }

    /**
     * Get the status model
     * @return the status model
     */
    public StatusModel getStatusModel() {
        return statusModel;
    }

    /**
     * Set the status model
     * @param statusModel the status model
     */
    public void setStatusModel(StatusModel statusModel) {
        this.statusModel = statusModel;
        updateLayout();
    }

    /**
     * Get the status view listener
     * @return the status view listener
     */
    public StatusViewListener getStatusViewListener() {
        return statusViewListener;
    }

    /**
     * Set a new status view listener
     * @param statusViewListener the new status view listener
     */
    public void setStatusViewListener(StatusViewListener statusViewListener) {
        this.statusViewListener = statusViewListener;
    }

    public interface StatusViewListener{
        void onSaveStatusButtonClick();
    }
}
