package Views;

import android.content.Context;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sony.wowapp_copy.R;

import java.io.IOException;

import POJO.Interfaces.OnChangeListener;
import POJO.Models.ProfileModel;
import Utils.Constants;

/**
 * Created by sony on 09/07/2015.
 */
public class ProfileLayout extends RelativeLayout implements OnChangeListener<ProfileModel> {


    /**
     * The profile picture image view
     */
    private ImageView profilePictureImageView;
    /**
     * The name text view containing the first and last name
     */
    private TextView nameTextView;
    /**
     * The status text view
     */
    private TextView statusTextView;
    /**
     * The username text view
     */
    private TextView usernameTextView;
    /**
     * The birthday text view
     */
    private TextView birthdayTextView;
    /**
     * The gender text view
     */
    private TextView genderTextView;
    /**
     * The country text view
     */
    private TextView countryTextView;
    /**
     * The city text view
     */
    private TextView cityTextView;
    /**
     * The email text view
     */
    private TextView emailTextView;
    /**
     * The phone number text view
     */
    private TextView phoneNumberTextView;
    /**
     * The phone type text view
     */
    private TextView phoneTypeTextView;
    /**
     * The city relative layout
     */
    RelativeLayout cityRelativeLayout;
    /**
     * The edit button
     */
    private Button editButton;
    /**
     * The profile model
     */
    private ProfileModel profileModel;
    /**
     * The view listener
     */
    private ViewListener viewListener;
    /**
     * The layout's context
     */
    Context context;

    /**
     * The profile layout's constructor
     *
     * @param context
     * @param attrs
     */
    public ProfileLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    /**
     * Initialize the profile layout's views
     */
    private void initLayout() {

        profilePictureImageView = (ImageView) findViewById(R.id.activity_profile_profile_picture_imageview);
        this.editButton = (Button) findViewById(R.id.activity_profile_edit_button);
        nameTextView = (TextView) findViewById(R.id.activity_profile_name_textview);
        usernameTextView = (TextView) findViewById(R.id.activity_profile_username_value_textview);
        statusTextView = (TextView) findViewById(R.id.activity_profile_status_textview);
        birthdayTextView = (TextView) findViewById(R.id.activity_profile_birthday_value_textview);
        genderTextView = (TextView) findViewById(R.id.activity_profile_gender_value_textview);
        cityRelativeLayout = (RelativeLayout) findViewById(R.id.activity_profile_relativelayout);
        emailTextView = (TextView) findViewById(R.id.activity_profile_email_value_textview);
        phoneNumberTextView = (TextView) findViewById(R.id.activity_profile_phone_number_textview);
        phoneTypeTextView = (TextView) findViewById(R.id.activity_profile_phone_textview);
        countryTextView = (TextView) findViewById(R.id.activity_profile_country_name_textview);
        cityTextView = (TextView) findViewById(R.id.activity_profile_city_name_textview);

        editButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().OnEditButtonClicked();
            }
        });
        statusTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().OnStatusTextViewClicked();
            }
        });
    }


    @Override
    public void onChange() {

        updateLayout();
    }

    /**
     * Update the layout
     */
    private void updateLayout() {
        statusTextView.setText(this.getProfileModel().getStatus());
        usernameTextView.setText(this.getProfileModel().getUsername());
        birthdayTextView.setText(Constants.simpleDateFormat_MMMddyyyy
                .format(this.getProfileModel().getBirthday()) + "");
        genderTextView.setText(this.getProfileModel().getGender());
        countryTextView.setText(this.getProfileModel().getCountry().getName());
        cityTextView.setText(this.getProfileModel().getCity());
        emailTextView.setText(this.getProfileModel().getEmail());
        phoneTypeTextView.setText("Phone (" + this.getProfileModel().getPhone().getPhoneType() + "): ");
        phoneNumberTextView.setText(this.getProfileModel().getPhone().getPhoneNumber());

        if (getProfileModel().getProfilePicture() != null) {
            try {
                profilePictureImageView.setLayoutParams(new LayoutParams(
                        profilePictureImageView.getLayoutParams().width, LayoutParams.MATCH_PARENT));
                profilePictureImageView.setBackgroundResource(0);
                profilePictureImageView.setImageBitmap(MediaStore.Images.Media.getBitmap(
                        this.context.getContentResolver(),
                        Uri.parse(getProfileModel().getProfilePicture())));
                nameTextView.setVisibility(View.INVISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            nameTextView.setVisibility(View.VISIBLE);
            profilePictureImageView.setBackgroundResource(R.drawable.circle);
            profilePictureImageView.setImageBitmap(null);
            nameTextView.setText(profileModel.getFirstName() + " " + profileModel.getLastName());
        }
    }

    /**
     * Set a new profile model
     *
     * @param profileModel the new profile model
     */
    public void setProfileModel(ProfileModel profileModel) {
        this.profileModel = profileModel;
        this.profileModel.addListener(this);
        updateLayout();

    }

    /**
     * Get the profile model
     *
     * @return the profile model
     */
    public ProfileModel getProfileModel() {
        return this.profileModel;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initLayout();
    }

    /**
     * Get the view listener
     *
     * @return the view listener
     */
    public ViewListener getViewListener() {
        return viewListener;
    }

    /**
     * Set a new view listener
     *
     * @param viewListener the new view listener
     */
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }


    public interface ViewListener {
        void OnEditButtonClicked();

        void OnStatusTextViewClicked();
    }
}
