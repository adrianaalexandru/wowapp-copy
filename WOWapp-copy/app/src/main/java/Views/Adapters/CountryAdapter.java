package Views.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.sony.wowapp_copy.R;

import java.util.ArrayList;

import POJO.Country;

/**
 * Created by sony on 09/07/2015.
 */
public class CountryAdapter extends BaseAdapter {
    /**
     * the context
     */
    private Context context;
    /**
     * the countries' list
     */
    private ArrayList<Country> items;
    /**
     * the layout inflater
     */
    private LayoutInflater layoutInflater;

    /**
     * constructor for the country adapter
     * @param context the context
     * @param items the countries list
     */
    public CountryAdapter(Context context, ArrayList<Country> items) {
        this.layoutInflater = ((Activity)context).getLayoutInflater();
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if(convertView==null) {
            convertView=layoutInflater.inflate(R.layout.country_layout,parent,false);

            holder=new ViewHolder();
            holder.countryNameTextView= (TextView) convertView.findViewById(R.id.country_layout_country_name_textview);
            holder.countryDialCodeTextView= (TextView) convertView.findViewById(R.id.country_layout_dial_code_textview);

            holder.countryCodeTextView= (TextView) convertView.findViewById(R.id.country_layout_country_code_textview);
            convertView.setTag(holder);

        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.countryNameTextView.setText(items.get(position).getName());
        holder.countryDialCodeTextView.setText(items.get(position).getDial_code());
        holder.countryCodeTextView.setText(items.get(position).getCode());
        return convertView;
    }
    public void setNewCountries(ArrayList<Country> newCountries) {
        this.items = newCountries;

    }


    private class ViewHolder {
        TextView countryDialCodeTextView;
        TextView countryNameTextView;
        TextView countryCodeTextView;
    }
}
