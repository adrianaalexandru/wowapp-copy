package Views;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.sony.wowapp_copy.R;

import POJO.Interfaces.OnChangeListener;
import POJO.Models.CountryModel;
import Views.Adapters.CountryAdapter;

/**
 * Created by sony on 09/07/2015.
 */
public class CountryLayout extends LinearLayout implements OnChangeListener <CountryModel>{

    /**
     * The country model
     */
    private CountryModel countryModel;
    /**
     * The list view of countries
     */
    private ListView countriesListView;
    /**
     * The country name edit text
     */
    private EditText countryNameEditText;
    /**
     * The add a new country floating action button
     */
    private FloatingActionButton addNewCountryFloatingActionButton;
    /**
     * The view listener
     */
    private ViewListener viewListener;
    /**
     * The country adapter
     */
    private CountryAdapter countryAdapter;

    /**
     * Country layout's constructor
     * @param context
     */
    public CountryLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Initialize the layout's views
     */

    public void initLayout() {
       countriesListView= (ListView) findViewById(R.id.activity_country_countries_listview);
       countryNameEditText= (EditText) findViewById(R.id.activity_country_country_name_edittext);
        addNewCountryFloatingActionButton=
                (FloatingActionButton) findViewById(R.id.activity_country_add_new_country_fab);

        addNewCountryFloatingActionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onAddNewCountryFloatingActionButtonClicked();
            }
        });
        countryNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                getViewListener().onTextChanged();
            }
        });
        countriesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getViewListener().onCountryItemClicked(position);
            }
        });

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initLayout();
    }

    /**
     * Get the country model
     * @return the country model
     */
    public CountryModel getCountryModel(){
        return this.countryModel;
    }

    /**
     * Set the country model
     * @param countryModel
     */
    public void setCountryModel(CountryModel countryModel){
      this.countryModel=countryModel;
      this.countryModel.addListener(this);
      updateView();
    }

    /**
     * Get the view listener
     * @return
     */
    public ViewListener getViewListener() {
        return viewListener;
    }
    /**
     * Set a new view listener
     * @param viewListener
     */
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    @Override
    public void onChange() {
        updateView();
    }

    /**
     * Get the country name edit text's content as a String
     * @return the country name edit text's content as a String
     */
    public String getCountryNameEditText(){
        return countryNameEditText.getText().toString();
    }

    /**
     * Get the country adapter
     * @return the country adapter
     */
    public CountryAdapter getCountryAdapter() {
        return countryAdapter;
    }
    /**
     * Update the the view
     */
    public void updateView(){
        if(countryAdapter != null) {
            countryAdapter.setNewCountries(this.countryModel.getCountryList());
        }
        else
        {
            countryAdapter=new CountryAdapter(getContext(),getCountryModel().getCountryList());
            this.countriesListView.setAdapter(countryAdapter);
        }
        countryAdapter.notifyDataSetChanged();
    }
    public interface ViewListener {
        void onTextChanged();
        void onCountryItemClicked(int position);
        void onAddNewCountryFloatingActionButtonClicked();
    }
}
