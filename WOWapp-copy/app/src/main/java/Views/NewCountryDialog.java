package Views;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.sony.wowapp_copy.R;

import POJO.Country;
import Managers.AppManager;

/**
 * Created by sony on 26/07/2015.
 */
public class NewCountryDialog extends Dialog {

    /**
     * The newcountry dialog's constructor
     *
     * @param context The context
     */
    public NewCountryDialog(Context context) {
        super(context);
    }

    public void dismiss() {
        super.dismiss();
    }

    public static class Builder {

        /**
         * The context
         */
        private final Context context;

        /**
         * The country name edit text
         */
        private EditText countryNameEditText;
        /**
         * The country code edit text
         */
        private EditText countryCodeEditText;
        /**
         * The dial code edit text
         */
        private EditText dialCodeEditText;
        /**
         * The ok button
         */
        private Button okButton;
        /**
         * The cancel button
         */
        private Button cancelButton;
        /**
         * The ok button clicked runnable
         */
        private Runnable onOkButtonClickedRunnable;


        /**
         * The builder constructor
         *
         * @param context The context
         */
        public Builder(Context context) {
            this.context = context;
        }

        /**
         * Build a new new country dialog
         *
         * @return the new country dialog
         */
        public NewCountryDialog build() {
            final NewCountryDialog result = new NewCountryDialog(getContext());
            result.requestWindowFeature(Window.FEATURE_NO_TITLE);

            result.setContentView(R.layout.new_country_custom_dialog_layout);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(result.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;

            result.getWindow().setAttributes(lp);
            initViews(result);
            return result;
        }

        /**
         * Initialize the views
         *
         * @param result
         */
        private void initViews(final NewCountryDialog result) {
            countryCodeEditText = (EditText) result.findViewById(R.id.new_country_custom_dialog_layout_countrycode_editext);
            countryNameEditText = (EditText) result.findViewById(R.id.new_country_custom_dialog_layout_countryname_editext);
            dialCodeEditText = (EditText) result.findViewById(R.id.new_country_custom_dialog_layout_dialcode_editext);

            okButton = (Button) result.findViewById(R.id.new_country_custom_dialog_layout_ok_button);
            cancelButton = (Button) result.findViewById(R.id.new_country_custom_dialog_layout_cancel_button);
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    result.cancel();
                }
            });
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((onOkButtonClickedRunnable != null) && (!verifyCountryName(getCountryNameEditText()))) {
                        onOkButtonClickedRunnable.run();
                        result.dismiss();
                    }
                }
            });
        }

        /**
         * Verify if there is already a country with the same name in the countries' list
         *
         * @param countryName the country name
         * @return true if there is a country with the same name in the countries' list,
         * false otherwise
         */
        private boolean verifyCountryName(String countryName) {
            Country country = new Country(countryName, "", "");
            if ((AppManager.getInstance().getCountryManager().getCountriesList().contains(country)) == true) {
                countryNameEditText.setError("A country with the same name already exists in the list of countries!");
                return true;
            }
            return false;
        }

        /**
         * Get the context
         *
         * @return the context
         */
        public Context getContext() {
            return context;
        }

        /**
         * Set a new on ok button clicked runnable
         *
         * @param onOkButtonClickedRunnable the new runnable
         * @return the builder
         */
        public Builder setOnOkButtonClicked(Runnable onOkButtonClickedRunnable) {
            this.onOkButtonClickedRunnable = onOkButtonClickedRunnable;
            return this;
        }

        /**
         * Get the country name edit text's content as a String
         *
         * @return the country name edit text's content as a String
         */
        public String getCountryNameEditText() {
            return countryNameEditText.getText().toString();
        }

        /**
         * Get the dial code edit text's content as a String
         *
         * @return the dial code edit text's content as a String
         */
        public String getDialCodeEditText() {
            return dialCodeEditText.getText().toString();
        }

        /**
         * Get the country code edit text's content as a String
         *
         * @return the country code edit text's content as a String
         */
        public String getCountryCodeEditText() {
            return countryCodeEditText.getText().toString();
        }
    }


}
