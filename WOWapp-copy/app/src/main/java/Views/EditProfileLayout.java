package Views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sony.wowapp_copy.R;

import java.io.IOException;

import POJO.Interfaces.OnChangeListener;
import POJO.Models.EditProfileModel;
import POJO.Phone;
import Utils.Constants;

/**
 * Created by sony on 12/07/2015.
 */
public class EditProfileLayout extends RelativeLayout implements OnChangeListener<EditProfileModel> {

    /**
     * The profile picture relative layout
     */
    private RelativeLayout profilePictureRelativeLayout;
    /**
     * The fist name edit text
     */
    private EditText firstnameEditText;
    /**
     * The last name edit text
     */
    private EditText lastnameEditText;
    /**
     * The birthday text view
     */
    private TextView birthdayTextView;
    /**
     * The gender textview
     */
    private TextView genderTextView;
    /**
     * The city edit text
     */
    private EditText cityEditText;
    /**
     * The country text view
     */
    private TextView countryTextView;
    /**
     * The phone number edit text
     */
    private EditText phoneNumberEditText;
    /**
     * The country code text view
     */
    private TextView countryCodeTextView;
    /**
     * The phone type text view
     */
    private TextView phoneTypeTextView;
    /**
     * The website edit text
     */
    private EditText websiteEditText;
    /**
     * The save button
     */
    private Button saveButton;
    /**
     * The back button
     */
    private Button backButton;
    /**
     * The layout's view listener
     */
    private ViewListener viewListener;
    /**
     * The layout's edit profile model
     */
    private EditProfileModel editProfileModel;

    /**
     * The edit profile's constructor
     *
     * @param context
     * @param attrs
     */
    public EditProfileLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Initialize the layout's views
     */
    private void initLayout() {

        this.birthdayTextView =
                (TextView) findViewById(R.id.activity_edit_profile_birthday_picker_textview);
        this.genderTextView =
                (TextView) findViewById(R.id.activity_edit_profile_gender_picker_textview);
        this.cityEditText =
                (EditText) findViewById(R.id.activity_edit_profile_city_picker_edittext);
        countryTextView =
                (TextView) findViewById(R.id.activity_edit_profile_country_picker_textview);
        phoneNumberEditText =
                (EditText) findViewById(R.id.activity_edit_profile_phone_picker_edittext);
        phoneTypeTextView =
                (TextView) findViewById(R.id.activity_edit_profile_phone_type_picker_textview);
        websiteEditText =
                (EditText) findViewById(R.id.activity_edit_profile_website_picker_editview);
        firstnameEditText =
                (EditText) findViewById(R.id.activity_edit_profile_firstname_edittext);
        lastnameEditText =
                (EditText) findViewById(R.id.activity_edit_profile_lastname_edittext);
        saveButton =
                (Button) findViewById(R.id.activity_edit_profile_save_button);
        profilePictureRelativeLayout =
                (RelativeLayout) findViewById(R.id.activity_edit_profile_profile_picture_relative_layout);
        backButton =
                (Button) findViewById(R.id.activity_edit_profile_back_button);
        countryCodeTextView =
                (TextView) findViewById(R.id.activity_edit_profile_country_code_textview);
        countryCodeTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onCountryCodeTextViewClicked();
            }
        });

        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onBackButtonClicked();
            }
        });
        saveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                getViewListener().onSaveButtonClicked();
            }
        });
        firstnameEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus)
                    getEditProfileModel().setFirstName(firstnameEditText.getText().toString(), true);
            }
        });
        lastnameEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus)
                    getEditProfileModel().setLastName(lastnameEditText.getText().toString(), true);
            }
        });
        cityEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus)
                    getEditProfileModel().setCity(cityEditText.getText().toString(), true);
            }
        });
        websiteEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus)
                    getEditProfileModel().setWebsite(websiteEditText.getText().toString(), true);
            }
        });
        phoneNumberEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus)
                    getEditProfileModel().setPhone(new Phone(phoneNumberEditText.getText().toString(), getEditProfileModel().getPhone().getPhoneType()), true);
            }
        });
        genderTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onGenderTextViewClicked();
            }
        });
        birthdayTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onBirthdayTextViewClicked();
            }
        });
        phoneTypeTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onPhoneTypeTextViewClicked();
            }
        });
        countryTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onCountryTextViewClicked();
            }
        });
        profilePictureRelativeLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onProfilePicturePickerClicked();
            }
        });

    }

    @Override
    public void onChange() {
        updateLayout();
    }

    /**
     * Update the layout
     */
    @TargetApi(16)
    private void updateLayout() {
        firstnameEditText.setText(this.getEditProfileModel().getFirstName());
        lastnameEditText.setText(this.getEditProfileModel().getLastName());
        birthdayTextView.setText(Constants.simpleDateFormat_ddMMyyyy
                .format(this.getEditProfileModel().getBirthday()));
        if (getEditProfileModel().getGender() != " ") {
            genderTextView.setText(getEditProfileModel().getGender());
        } else {
            genderTextView.setText("Male");
        }
        cityEditText.setText(getEditProfileModel().getCity());
        countryTextView.setText(getEditProfileModel().getCountry().getName());
        websiteEditText.setText(getEditProfileModel().getWebsite());
        phoneNumberEditText.setText(getEditProfileModel().getPhone().getPhoneNumber());
        countryCodeTextView.setText(getEditProfileModel().getCountry().getCode());
        phoneNumberEditText.setText(getEditProfileModel().getCountry().getDial_code());

        if (getEditProfileModel().getPhone().getPhoneType() == "") {
            phoneTypeTextView.setText("Mobile");
        } else {
            phoneTypeTextView.setText(getEditProfileModel().getPhone().getPhoneType());
        }
        if (getEditProfileModel().getProfilePicture() != null) {
            try {
                profilePictureRelativeLayout
                        .setBackground(new BitmapDrawable(MediaStore.Images.Media.getBitmap(super
                                .getContext().getContentResolver(), Uri.parse(getEditProfileModel()
                                .getProfilePicture()))));

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            profilePictureRelativeLayout.setBackgroundColor(super.getContext().getResources().getColor(R.color.Pink));
        }

    }

    /**
     * Set a new value for the edit profileModel field
     *
     * @param editProfileModel the new edit profile model
     */
    public void setEditProfileModel(EditProfileModel editProfileModel) {
        this.editProfileModel = editProfileModel;
        this.editProfileModel.addListener(this);
        updateLayout();
    }

    /**
     * Get the edit profile model
     *
     * @return the edit profile model
     */
    public EditProfileModel getEditProfileModel() {
        return editProfileModel;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initLayout();
    }

    /**
     * Get the view listener
     *
     * @return the view listener
     */
    public ViewListener getViewListener() {
        return viewListener;
    }

    /**
     * Set a new view listener
     *
     * @param viewListener the new view listener
     */
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    /**
     * Get the first name edit text's content as a String
     *
     * @return the first name edit text's content as a String
     */
    public String getFirstNameEditText() {
        return this.firstnameEditText.getText().toString();
    }

    /**
     * Get the last name edit text's content as a String
     *
     * @return the last name edit text's content as a String
     */
    public String getLastNameEditText() {
        return this.lastnameEditText.getText().toString();
    }

    /**
     * Get the gender text view's content as a String
     *
     * @return the gender text view's content as a String
     */
    public String getGenderTextView() {
        return this.genderTextView.getText().toString();
    }

    /**
     * Get the website edit text's content as a String
     *
     * @return the website edit text's content as a String
     */
    public String getWebsiteEditText() {
        return this.websiteEditText.getText().toString();
    }

    /**
     * Get the phone number edit text's content as a String
     *
     * @return the phone number edit text's content as a String
     */
    public String getPhoneNumberEditText() {
        return this.phoneNumberEditText.getText().toString();
    }

    /**
     * Get the phone type text view's content as a String
     *
     * @return the phone type text view's content as a String
     */
    public String getPhoneTypeTextView() {
        return this.phoneTypeTextView.getText().toString();
    }

    /**
     * Get the city edit text view's content as a String
     *
     * @return the city edit text view's content as a String
     */
    public String getCityEditText() {
        return this.cityEditText.getText().toString();
    }

    /**
     * Get the birthday text view's content as a String
     *
     * @return the birthday text view's content as a String
     */
    public String getBirthdayTextView() {
        return this.birthdayTextView.getText().toString();
    }

    public interface ViewListener {
        void onBirthdayTextViewClicked();

        void onGenderTextViewClicked();

        void onCountryTextViewClicked();

        void onPhoneTypeTextViewClicked();

        void onSaveButtonClicked();

        void onProfilePicturePickerClicked();

        void onBackButtonClicked();

        void onCountryCodeTextViewClicked();
    }
}
