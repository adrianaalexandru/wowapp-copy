package Views;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.sony.wowapp_copy.R;

import java.util.ArrayList;

import static com.example.sony.wowapp_copy.R.id.notifications_custom_dialog_layout_cancel_button;
import static com.example.sony.wowapp_copy.R.id.notifications_custom_dialog_layout_ok_button;

/**
 * Created by sony on 22/07/2015.
 */
public class NotificationDialog extends Dialog {


    /**
     * The notification dialog's constructor
     * @param context The context
     */
    public NotificationDialog(Context context) {
        super(context);
    }

    public void dismiss(){
        super.dismiss();
    }


    public static class Builder {

        /**
         * The context
         */
        private final Context context;

        /**
         * The title text view
         */
        private TextView titleTextView;
        /**
         * The vibrate view group
         */
        private ViewGroup vibrateViewGroup;
        /**
         * The sound view group
         */
        private ViewGroup soundViewGroup;
        /**
         * The light view group
         */
        private ViewGroup lightViewGroup;
        /**
         * The vibrate checkbox
         */
        private CheckBox vibrateCheckBox;
        /**
         * The sound checkbox
         */
        private CheckBox soundCheckBox;
        /**
         * The light checkbox
         */
        private CheckBox lightCheckBox;
        /**
         * The ok button
         */
        private Button okButton;
        /**
         * The cancel button
         */
        private Button cancelButton;
        /**
         * The ok button clicked runnable
         */
        private Runnable onOkButtonClickedRunnable;

        /**
         * The title
         */
        private String title;
        /**
         * The notifications' list
         */
        private ArrayList<String> notificationsList;


        /**
         * The constructor for builder
         * @param context
         */
        public Builder(Context context) {
            this.context = context;
        }

        /**
         * Build a new notification dialog
         * @return the notification dialog
         */
        public NotificationDialog build(){
            final NotificationDialog result=new NotificationDialog(getContext());
            result.requestWindowFeature(Window.FEATURE_NO_TITLE);

            result.setContentView(R.layout.notifications_custom_dialog_layout);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(result.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;

            result.getWindow().setAttributes(lp);
            initViews(result);
            return result;
        }

        /**
         * Initialize the views
         * @param result
         */
        private void initViews(final NotificationDialog result) {


            titleTextView= (TextView) result.findViewById(R.id.notifications_custom_dialog_layout_title_textview);
            titleTextView.setText(title);

            vibrateViewGroup= (ViewGroup) result.findViewById(R.id.notifications_custom_dialog_layout_vibrate_viewgroup);
            soundViewGroup= (ViewGroup) result.findViewById(R.id.notifications_custom_dialog_layout_sound_viewgroup);
            lightViewGroup= (ViewGroup) result.findViewById(R.id.notifications_custom_dialog_layout_light_viewgroup);

            ((TextView)vibrateViewGroup.findViewById(R.id.notification_layout_notification_type_textview)).setText(result.getContext().getResources().getString(R.string.Vibrate));
            ((TextView)soundViewGroup.findViewById(R.id.notification_layout_notification_type_textview)).setText(result.getContext().getResources().getString(R.string.Sound));
            ((TextView)lightViewGroup.findViewById(R.id.notification_layout_notification_type_textview)).setText(result.getContext().getResources().getString(R.string.Light));

            vibrateCheckBox=(CheckBox)vibrateViewGroup.findViewById(R.id.notification_layout_checkbox);
            soundCheckBox=(CheckBox)soundViewGroup.findViewById(R.id.notification_layout_checkbox);
            lightCheckBox=(CheckBox)lightViewGroup.findViewById(R.id.notification_layout_checkbox);
            if(notificationsList.contains(result.getContext().getResources().getString(R.string.Vibrate))==true)
                vibrateCheckBox.setChecked(true);
            if(notificationsList.contains(result.getContext().getResources().getString(R.string.Light))==true)
                lightCheckBox.setChecked(true);
            if(notificationsList.contains(result.getContext().getResources().getString(R.string.Sound))==true)
                soundCheckBox.setChecked(true);

            vibrateViewGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    vibrateCheckBox.setChecked(!vibrateCheckBox.isChecked());
                }
            });
            lightViewGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lightCheckBox.setChecked(!lightCheckBox.isChecked());
                }
            });
            soundViewGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    soundCheckBox.setChecked(!soundCheckBox.isChecked());

                }
            });
            okButton= (Button) result.findViewById(notifications_custom_dialog_layout_ok_button);
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notificationsList.clear();
                    if(vibrateCheckBox.isChecked()==true)
                        notificationsList.add(result.getContext().getResources().getString(R.string.Vibrate));
                    if(soundCheckBox.isChecked()==true)
                        notificationsList.add(result.getContext().getResources().getString(R.string.Sound));
                    if(lightCheckBox.isChecked()==true)
                        notificationsList.add(result.getContext().getResources().getString(R.string.Light));
                    if(onOkButtonClickedRunnable!=null) {
                        onOkButtonClickedRunnable.run();
                    }
                    result.dismiss();
                }
            });
           cancelButton= (Button) result.findViewById(notifications_custom_dialog_layout_cancel_button);
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    result.cancel();
                }
            });
        }
        /**
         * Get the context
         * @return the context
         */
        public Context getContext() {
            return context;
        }

        /**
         * Set a new title
         * @param title the new title
         */
        public void setTitle(String title) {
            this.title = title;
        }

        /**
         * Get the title
         * @return the title
         */
        public String getTitle(){
            return this.title;
        }

        /**
         * Get the notifications' list
         * @return the notifications' list
         */
        public ArrayList<String> getNotificationsList(){
            return this.notificationsList;
        }

        /**
         * Set a new notifications' list
         * @param notificationsList the new list
         */
        public void setNotificationsList(ArrayList<String> notificationsList) {
            this.notificationsList = notificationsList;
        }

        /**
         * Set a new on ok button clicked runnable
         * @param onOkButtonClickedRunnable
         * @return
         */
        public Builder setOnOkButtonClicked(Runnable onOkButtonClickedRunnable) {
            this.onOkButtonClickedRunnable = onOkButtonClickedRunnable;
            return this;
        }
    }
}
