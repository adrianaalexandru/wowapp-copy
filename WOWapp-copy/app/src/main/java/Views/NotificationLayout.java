package Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sony.wowapp_copy.R;

import java.util.ArrayList;

import POJO.Interfaces.OnChangeListener;
import POJO.Models.NotificationModel;

/**
 * Created by sony on 16/07/2015.
 */
public class NotificationLayout extends LinearLayout implements OnChangeListener<NotificationModel> {

    /**
     * The network notifications text view
     */
    TextView networkNotificationsTextView;
    /**
     * The earnings notifications text view
     */
    TextView earningsNotificationsTextView;
    /**
     * The calls notifications text view
     */
    TextView callsNotificationsTextView;
    /**
     * The messages notifications text view
     */
    TextView messagesNotificationsTextView;
    /**
     * The messages view group
     */
    private ViewGroup messagesViewGroup;
    /**
     * The calls view group
     */
    private ViewGroup callsViewGroup;
    /**
     * The earnings view group
     */
    private ViewGroup earningsViewGroup;
    /**
     * The network view group
     */
    private ViewGroup networkViewGroup;
    /**
     * The notifications model
     */
    private NotificationModel notificationModel;
    /**
     * The view listener
     */
    private NotificationLayout.ViewListener viewListener;


    /**
     * The VibrateSoundLightLayout's constructor
     *
     * @param context
     * @param attrs
     */
    public NotificationLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Initialize the layout
     */
    private void initLayout() {
        networkViewGroup = (ViewGroup) findViewById(R.id.activity_notifications_network_viewgroup);
        callsViewGroup = (ViewGroup) findViewById(R.id.activity_notifications_calls_viewgroup);
        earningsViewGroup = (ViewGroup) findViewById(R.id.activity_notifications_earnings_viewgroup);
        messagesViewGroup = (ViewGroup) findViewById(R.id.activity_notifications_messages_viewgroup);

        networkNotificationsTextView = (TextView) networkViewGroup
                .findViewById(R.id.notifications_item_layout_subtitle_textview);
        callsNotificationsTextView = (TextView) callsViewGroup
                .findViewById(R.id.notifications_item_layout_subtitle_textview);
        earningsNotificationsTextView = (TextView) earningsViewGroup
                .findViewById(R.id.notifications_item_layout_subtitle_textview);
        messagesNotificationsTextView = (TextView) messagesViewGroup
                .findViewById(R.id.notifications_item_layout_subtitle_textview);

        ((TextView) networkViewGroup.findViewById(R.id.notifications_item_layout_title_textview))
                .setText(getResources().getString(R.string.Network));
        ((TextView) callsViewGroup.findViewById(R.id.notifications_item_layout_title_textview))
                .setText(getResources().getString(R.string.Calls));
        ((TextView) earningsViewGroup.findViewById(R.id.notifications_item_layout_title_textview))
                .setText(getResources().getString(R.string.Earnings));
        ((TextView) earningsViewGroup.findViewById(R.id.notifications_item_layout_title_textview))
                .setText(getResources().getString(R.string.Messages));

        networkViewGroup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onNetworkLinearLayoutClicked();
            }
        });
        callsViewGroup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onCallsLinearLayoutClicked();
            }
        });
        earningsViewGroup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onEarningsLinearLayoutClicked();
            }
        });
        messagesViewGroup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onMessagesLinearLayoutClicked();
            }
        });
    }

    @Override
    public void onChange() {
        updateLayout();
    }

    private void updateLayout() {
        updateNotificationsItemsLayout(messagesNotificationsTextView,
                getNotificationModel().getMessagesNotifications());
        updateNotificationsItemsLayout(callsNotificationsTextView,
                getNotificationModel().getCallsNotifications());
        updateNotificationsItemsLayout(earningsNotificationsTextView,
                getNotificationModel().getEarningsNotifications());
        updateNotificationsItemsLayout(networkNotificationsTextView,
                getNotificationModel().getNetworkNotifications());

    }

    /**
     * Update the text of the notifications items text view with the elements from the notificationsList array list
     * If the array list is empty, the text view's text will become "No notification"
     *
     * @param notificationsItemsTextView the text view whose text content will be updated
     * @param notificationsList          the list of notifications
     */
    private void updateNotificationsItemsLayout(TextView notificationsItemsTextView,
                                                ArrayList<String> notificationsList) {
        if (notificationsList.size() != 0) {
            String notificationsText = "";
            for (int i = 0; i < notificationsList.size(); i++) {
                if (i != 0)
                    notificationsText += ", ";
                notificationsText += notificationsList.get(i);
            }
            notificationsItemsTextView.setText(notificationsText);
        } else
            notificationsItemsTextView.setText("No notification");
    }

    /**
     * Get the notifications model
     *
     * @return
     */
    public NotificationModel getNotificationModel() {
        return notificationModel;
    }

    /**
     * Set a new notifications model
     *
     * @param notificationModel the new notifications model
     */
    public void setNotificationModel(NotificationModel notificationModel) {
        this.notificationModel = notificationModel;
        this.notificationModel.addListener(this);
        updateLayout();
    }

    /**
     * Get the view listener
     *
     * @return the view listener
     */
    public ViewListener getViewListener() {
        return viewListener;
    }

    /**
     * Set a new view listener
     *
     * @param viewListener a new view listener
     */
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initLayout();
    }

    public interface ViewListener {
        void onMessagesLinearLayoutClicked();

        void onCallsLinearLayoutClicked();

        void onEarningsLinearLayoutClicked();

        void onNetworkLinearLayoutClicked();
    }
}
